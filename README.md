
# Pasos del curso


una breve descripción de los pasos de esta semana

## Instalación
Via npm
```bash
    npm i --save copyfiles
    npm i --save cssmin
    npm i --save htmlmin
    npm i --save imagemin-cli
    npm i --save lite-server
    npm i --save node-sass
    npm i --save onchange
    npm i --save rimraf
    npm i --save  uglify-js
    npm i --save usemin-cli
    npm i --save-dev bootstrap
    npm i --save-dev concurrently
    npm i --save-dev jquery
    npm i --save-dev less
    npm i --save-dev open-icon
    npm i --save-dev open-iconic
    npm i --save-dev popper.js
    npm i --save-dev sass
```
## Grunt
```bash
    npm i --save-dev grunt
    npm i --save-dev grunt-contrib-sass
    npm i --save-dev grunt-contrib-watch
    npm i --save-dev grunt-browser-sync
```

## Gulp
```bash
    npm i --global gulp-cli
    npm i --save-dev gulp
    npm i --save-dev gulp-sass
    npm i --save-dev browser-sync
    npm i --save-dev del
    npm i --save-dev gulp-imagemin
    npm i --save-dev gulp-uglify
    npm i --save-dev gulp-usemin
    npm i --save-dev gulp-rev
    npm i --save-dev gulp-clean-css
    npm i --save-dev gulp-flatmap
    npm i --save-dev gulp-htmlmin
    
```

## Script en package.json
```bash
    "dev": npx lite-server
    "scss": node-sass -o css/ css/
    "less": npx lessc css/style2.less css/style2.css
    "watch:scss": onchange css/*.scss -- npm run scss
    "start": concurrently \"npm run watch:scss\" \"npm run dev\"
    "clean": npx rimraf dist
    "imagemin": npx imagemin assets/* --out-dir dist/assets
    "usemin": usemin index.html -d dist --htmlmin -o dist/index.html
    "build": "npm run clean && npm run scss && npm run imagemin && npm run usemin"
```
## adicional en los html agregar

```bash
nos indica la salida del archivo e inicio del proceso
    <!-- build:css css/index.css -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <!-- endbuild -->

    <!-- build:js js/index.js -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        $(() => {
            $('[data-toggle="tooltip"]').tooltip()
            $('[data-toggle="popover"]').popover()
            $('.carousel').carousel({
                interval: 5000
            })
        })
        $('#btnCancelFormModal').on('click', () => {
            $('#modalSubscribe').modal('hide')
        })
        
        $('#modalSubscribe').on('shown.bs.modal', () => {
            $('#inputModalInit').val($('#inputSubscribeInit').val())
        })
        $('#modalSubscribe').on('hide.bs.modal', () => {
            $('#inputModalInit').val('')
            $('#inputNameModalInit').val('')
        })
    </script>
    <!-- endbuild -->
```
## Por último correr
```bash
    npm run build
```

