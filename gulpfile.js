'use strict';

// Este cambio tambien se requirio por temas de los plugins actualizados y añadir en el package te  "type": "commonjs",
import {
    deleteAsync
} from 'del';
import gulp from 'gulp';
import browserSync from 'browser-sync';
import uglify from 'gulp-uglify';
import usemin from 'gulp-usemin';
import cleanCss from 'gulp-clean-css';
import flatmap from 'gulp-flatmap';
import htmlmin from 'gulp-htmlmin';
import imagemin from 'gulp-imagemin';
import rev from 'gulp-rev';

gulp.task('scss', () => {
    return gulp.src('./css/*.scss')
        .pipe(scss().on('error', scss.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('scss:watch', () => {
    gulp.watch(['./css/*.scss'], gulp.series('scss'));
});

gulp.task('browser-sync', () => {
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

// Se agrega este cambio al curso ya que la documentación asi lo requiere
gulp.task('default', gulp.parallel('scss:watch', 'browser-sync'));

///sector de deploy

gulp.task('clean', () => {
    return deleteAsync(['dist']);
});

gulp.task('copyfonts', (cb) => {
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('dist/fonts'))
    cb()
});

gulp.task('imagemin', () => {
    return gulp.src('./assets/*.{png,jpg,gif,jpeg,webp}')
        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('usemin', () => {
    return gulp.src('./*.html')
        .pipe(flatmap((stream, _) => {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [() => {
                        return htmlmin({
                            collapseWhitespace: true
                        })
                    }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series(['clean', 'copyfonts', 'imagemin', 'usemin']));