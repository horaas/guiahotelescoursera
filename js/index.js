$(() => {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2000
    })
})
$('#btnCancelFormModal').on('click', () => {
    $('#modalSubscribe').modal('hide')
})

$('#modalSubscribe').on('show.bs.modal', () => {
    $('#btnOpenModalSubscribe').addClass('btn-danger').attr('disabled', true)
})

$('#modalSubscribe').on('shown.bs.modal', () => {
    $('#inputModalInit').val($('#inputSubscribeInit').val())
})
$('#modalSubscribe').on('hide.bs.modal', () => {
    $('#btnOpenModalSubscribe').removeClass('btn-danger').attr('disabled', false)
})
$('#modalSubscribe').on('hidden.bs.modal', () => {
    $('#inputModalInit').val('')
    $('#inputNameModalInit').val('')
})